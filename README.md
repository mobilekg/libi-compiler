# libi compiler
<a id="top" name="top"></a>

1. [Description](#description)
2. [Dependencies](#dependencies)
3. [Scripts](#scripts)
4. [Outputs](#outputs)
5. [References](#references)

## [Description](#description) 
The following three PowerShell scripts can be executed on the command line, or be easily integrated into the AppVeyor CI pipeline through the configuration file `appveyor.yml`: 
- `local-prepare.ps1` => install section in `appveyor.yml`
- `local-build.ps1` => build_script section in `appveyor.yml`
- `local-test.ps1` => test_script section in `appveyor.yml`
<br/>

<div align="right"> <a href="#top">↥ back to top</a> </div>

## [Dependencies](#dependencies)

<div align="right"> <a href="#top">↥ back to top</a> </div>

## [Scripts](#scripts)
- `local-utility.ps1`
  > The configuration & utility file is shared by all the PowerShell scripts
- `local-prepare.ps1`
  > This script installs dependent packages, sets up envirement variables
- `local-build.ps1`
  > Build script
- `local-test.ps1`
  > Test script
- `appveyor.yml`
  > AppVeyor configuration file
  
<div align="right"> <a href="#top">↥ back to top</a> </div>

## [Outputs](#outputs)

<div align="right"> <a href="#top">↥ back to top</a> </div>

## [References](#references)

<div align="right"> <a href="#top">↥ back to top</a> </div>
