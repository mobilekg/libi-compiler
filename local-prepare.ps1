
# Load utility
. ".\local-utility.ps1"

# Check MSYS2 installation
$cmd = $MSYS2_FOLDER + "\msys2_shell.cmd"
if(![System.IO.File]::Exists($cmd)){
    Write-Host (">> {0} does not seem to be the root folder of an existing MSYS2 installation" -f $MSYS2_FOLDER)
    exit
}

# # Update MSYS2
# ExecuteMSYS2Cmd("pacman -Syuu --noconfirm")

# # Install dependencies
# ExecuteMSYS2Cmd("pacman -S base-devel --noconfirm")

# Clone the code
$cloneRepo = "https://github.com/libimobiledevice/libplist.git"
Write-Host (">> cloneRepo: {0}" -f $cloneRepo)
$curPath = $PSScriptRoot.replace("\","/")
$cloneFolder = $curPath + "/libplist"
Write-Host (">> cloneFolder: {0}" -f $cloneFolder)
# $cmd = ("git clone --single-branch --branch master {0} {1}" -f $cloneRepo, $cloneFolder)
# ExecuteCmd($cmd)
CloneRepo $cloneRepo $cloneFolder

ExecuteMSYS2Cmd($cloneFolder + "/autogen.sh")
ExecuteMSYS2Cmd($cloneFolder + "/make")
ExecuteMSYS2Cmd($cloneFolder + "/make install")

# # Display all environment variables
# gci env:* | sort-object name