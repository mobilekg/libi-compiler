

$Global:MSYS2_FOLDER = "C:\msys64"
# $Global:MSYS2_FOLDER = "D:\dev\msys64"
$Global:BASH_EXE = $MSYS2_FOLDER + "\usr\bin\bash.exe"

function ExecuteCmd($cmd)
{
    Write-Host (">> Execute {0}" -f $cmd)
    [Diagnostics.Process]::Start($cmd)
    Write-Host (">> Finish {0}" -f $cmd)
}

# MSYS2 (C:\msys64)
# Example: C:\msys64\usr\bin\bash.exe -l -c "pacman -Syuu"
function ExecuteMSYS2Cmd($cmd)
{
    Write-Host (">> Execute {0}" -f $cmd)

    $prms = ('-l -c "{0}"' -f $cmd)
    $prms = $prms.Split(" ")
    & "$BASH_EXE" $prms
    
    Write-Host (">> Finish {0}" -f $cmd)
}

function CloneRepo($cloneRepo, $cloneFolder)
{
    Write-Host (">> CloneRepo from {0} to {1}" -f $cloneRepo, $cloneFolder)
    $timeoutSeconds = 180
    $attempts = 2

    while($attempts-- -gt 0) {
        if(Test-Path $cloneFolder) {
            Write-Host "Cleaning up ..."
            Get-ChildItem -Path $cloneFolder -Include * -Hidden -Recurse | foreach { Remove-Item $_.FullName -Force -Recurse }
        } else {
            Write-Host "Creating folder..."
            New-Item $cloneFolder -ItemType directory | Out-Null        
        }

        Write-Host "Cloning..."

        $si = new-object System.Diagnostics.ProcessStartInfo
        $si.UseShellExecute = $false
        $si.FileName = "git"
        $si.Arguments = "clone $cloneRepo `"$cloneFolder`""
        $p = [diagnostics.process]::Start($si)

        if($p.WaitForExit($timeoutSeconds * 1000)) {
            Write-Host "Clone success"
            break 
        } else {
            # stuck
            Write-Host "git clone has stuck." -ForegroundColor Yellow
            Write-Host "Terminating git process..."
            cmd /c taskkill /PID $p.Id /F /T
            Start-Sleep -s 5
        }
    }
}